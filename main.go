package moosql

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

var (
	dbServer = ""
)

func SetDBServer(dbServe string) {
	dbServer = dbServe
}

func GetSQL() (*sql.DB, error) {
	con, err := sql.Open("sqlite3", dbServer)
	if err != nil {
		return con, err
	}

	return con, nil
}
